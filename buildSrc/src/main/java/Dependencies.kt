
object Versions {
    const val compile_sdk_version = 27
    const val min_sdk_version = 19
    const val target_sdk_version = 27

    const val version_code = 1
    const val version_name = "1.0"

    const val kotlin_version = "1.2.70"
    const val android_gradle_plugin = "3.1.4"

    const val support_lib = "27.1.1"

    const val constraint_layout = "1.1.3"

    const val retrofit = "2.4.0"

    const val dagger = "2.15"

    const val rxjava = "2.1.13"
    const val rxAndroid = "2.0.2"

    const val archi_extensions = "1.1.1"

    const val data_binding = "2.3.1"

    const val play_services = "15.0.1"

    const val room_version = "1.1.1"

    const val mockito_inline = "2.11.0"

    const val junit = "4.12"
    const val support_test = "1.0.2"
}

object Dependencies {
    const val kotlin_stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin_version}"
    const val kotlin_gradle_plugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin_version}"
    const val android_gradle_plugin = "com.android.tools.build:gradle:${Versions.android_gradle_plugin}"

    const val support_lib = "com.android.support:appcompat-v7:${Versions.support_lib}"
    const val support_design = "com.android.support:design:${Versions.support_lib}"
    const val support_vector_drawable = "com.android.support:support-vector-drawable:${Versions.support_lib}"
    const val constraint_layout = "com.android.support.constraint:constraint-layout:${Versions.constraint_layout}"
    const val recycler_view = "com.android.support:recyclerview-v7:${Versions.support_lib}"
    const val card_view = "com.android.support:cardview-v7:${Versions.support_lib}"

    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val retrofit_gson_converter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    const val retrofit_adapter_rxjava = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit}"

    const val rx_java = "io.reactivex.rxjava2:rxjava:${Versions.rxjava}"
    const val rx_android = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroid}"

    const val dagger =  "com.google.dagger:dagger:${Versions.dagger}"
    const val dagger_compiler =  "com.google.dagger:dagger-compiler:${Versions.dagger}"
    const val dagger_android_processor = "com.google.dagger:dagger-android-processor:${Versions.dagger}"
    const val dagger_support = "com.google.dagger:dagger-android-support:${Versions.dagger}"

    const val archi_extensions = "android.arch.lifecycle:extensions:${Versions.archi_extensions}"
    const val archi_annotation_processor = "android.arch.lifecycle:compiler:${Versions.archi_extensions}"
    const val archi_core_testing = "android.arch.core:core-testing:${Versions.archi_extensions}"

    const val data_binding = "com.android.databinding:compiler:${Versions.data_binding}"

    const val google_maps = "com.google.android.gms:play-services-maps:${Versions.play_services}"

    const val room = "android.arch.persistence.room:runtime:${Versions.room_version}"
    const val room_compiler =  "android.arch.persistence.room:compiler:${Versions.room_version}"
    const val room_rxjava = "android.arch.persistence.room:rxjava2:${Versions.room_version}"

    const val mockito_inline = "org.mockito:mockito-inline:${Versions.mockito_inline}"

    const val support_test_runner = "com.android.support.test:runner:${Versions.support_test}"
    const val support_test_rules = "com.android.support.test:rules:${Versions.support_test}"

    const val junit = "junit:junit:${Versions.junit}"
}
