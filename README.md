# WunderAssignment

Screen 1:

Displays all the cars information in a list.

Screen 2:

Display all the cars on the map.

Selecting the car in the list opens the map screen, zooms on that car’s
location and displays its name on top of the map pin.

Selecting the car on the map also display its name on top of the map pin.


## Environment

 Android Studio = 3.1.4

 Kotlin = 1.2.70

## Structure

 MVVM pattern along with Android architecture components is used to develop this project with repository pattern.
 Project structure is scalable and testable

## Libraries

  LiveData ViewModel, Room (For persistence) ,Retrofit2, Dagger, RxJava, Google Map

## Dependency Management

  Used buildSrc with kotlin DSL plugin to create autoComplete and easy to navigate dependencies
  https://caster.io/lessons/gradle-dependency-management-using-kotlin-and-buildsrc-for-buildgradle-autocomplete-in-android-studio


## How to Build
  Import the project in Android Studio
  To use maps (Add SHA-1 and package name to Google project and replace the API key in google_maps_api.xml)



