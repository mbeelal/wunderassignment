package com.wunder.assignment

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.wunder.assignment.view.map.MapsFragment
import com.wunder.assignment.view.vehicles.CarsFragment
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

const val EXTRA_ID = "SelectedMenuId"

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var mDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return mDispatchingAndroidInjector
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_cars -> {
                loadFragment(CarsFragment.newInstance())
                updateTitle(getString(R.string.title_cars))
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_map -> {
                loadFragment(MapsFragment.newInstance())
                updateTitle(getString(R.string.title_map))
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var defaultMenuId = R.id.navigation_cars
        savedInstanceState?.let {
            defaultMenuId = savedInstanceState.getInt(EXTRA_ID)
        } ?: kotlin.run {
            defaultMenuId = intent.getIntExtra(EXTRA_ID, R.id.navigation_cars)
        }

        navigation.apply {
            setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
            selectedItemId = defaultMenuId
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putInt(EXTRA_ID, navigation.selectedItemId)
    }

    private fun updateTitle(title: String) {
        supportActionBar?.let {
            it.title = title
        }
    }

    private fun loadFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.container, fragment)
            commit()
        }
    }

    fun openMapFragment() {
        navigation.selectedItemId = R.id.navigation_map
    }
}
