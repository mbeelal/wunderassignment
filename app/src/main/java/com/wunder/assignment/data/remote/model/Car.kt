package com.wunder.assignment.data.remote.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Entity(tableName = "Car")
@Parcelize
data class Car(
        @PrimaryKey(autoGenerate = true)
        val id: Int,
        @ColumnInfo(name = "address")
        val address: String = "",
        @ColumnInfo(name ="fuel")
        val fuel: Int = 0,
        @ColumnInfo(name = "exterior")
        val exterior: String = "",
        @ColumnInfo(name = "coordinates")
        val coordinates: List<Double>,
        @ColumnInfo(name = "name")
        val name: String = "",
        @ColumnInfo(name = "engineType")
        val engineType: String = "",
        @ColumnInfo(name = "vin")
        val vin: String = "",
        @ColumnInfo(name = "interior")
        val interior: String = ""): Parcelable