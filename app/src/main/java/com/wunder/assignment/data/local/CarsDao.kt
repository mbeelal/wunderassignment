package com.wunder.assignment.data.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.wunder.assignment.data.remote.model.Car
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface CarsDao {

    @Query("SELECT * FROM Car")
    fun getVehicles(): Flowable<List<Car>>

    @Query("SELECT * FROM Car WHERE id= :id")
    fun getVehicleById(id: Int): Single<Car>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveVehicles(vehicles: List<Car>)

    @Query("DELETE from Car")
    fun deleteVehicles()
}