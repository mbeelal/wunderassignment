package com.wunder.assignment.data.remote.model

import com.google.gson.annotations.SerializedName

data class Placemarks(@SerializedName("placemarks")
                      val placemarks: List<Car>)