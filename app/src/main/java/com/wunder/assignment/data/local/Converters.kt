package com.wunder.assignment.data.local

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson

class Converters {

    @TypeConverter
    fun listToJson(value: List<Double>?): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun jsonToList(value: String): List<Double>? {
        val objects = Gson().fromJson(value, Array<Double>::class.java) as Array<Double>
        return objects.toList()
    }
}