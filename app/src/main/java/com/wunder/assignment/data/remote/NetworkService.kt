package com.wunder.assignment.data.remote

import com.wunder.assignment.data.remote.model.Placemarks
import com.wunder.assignment.util.LOCATIONS_JSON
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET


/**
 * Interface for API
 */
interface NetworkService {

    @GET(LOCATIONS_JSON)
    fun getLocationsJson(): Single<Response<Placemarks>>
}