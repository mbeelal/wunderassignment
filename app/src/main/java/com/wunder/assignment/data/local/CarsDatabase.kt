package com.wunder.assignment.data.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.wunder.assignment.data.remote.model.Car

@Database(entities = [Car::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class CarsDatabase: RoomDatabase() {
    abstract fun getVehiclesDao(): CarsDao
}