package com.wunder.assignment.repository

import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.wunder.assignment.data.local.CarsDao
import com.wunder.assignment.data.remote.NetworkService
import com.wunder.assignment.data.remote.model.Placemarks
import com.wunder.assignment.data.remote.model.Car
import com.wunder.assignment.util.NetworkBoundResource
import com.wunder.assignment.util.Resource
import com.wunder.assignment.util.SchedulerProvider
import io.reactivex.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CarsRepository @Inject constructor(private val networkService: NetworkService,
                                         private val vehiclesDao: CarsDao,
                                         private val context: Context,
                                         private val schedulerProvider: SchedulerProvider) {


    fun getVehiclesFromDatabase(): Flowable<List<Car>> = vehiclesDao.getVehicles()

    //fetch data from API
    fun getVehicleById(id: Int): MutableLiveData<Car> {
        val data = MutableLiveData<Car>()

        vehiclesDao.getVehicleById(id)
                .compose(schedulerProvider.ioToMainSingleScheduler())
                .subscribe(
                        { data.value = it },
                        {
                            //TODO: Error need to be handled/propagated
                        }
                )
        return data
    }

//
    fun loadVehiclesData(): Flowable<Resource<List<Car>>> {
        return object: NetworkBoundResource<List<Car>, Placemarks>(context, schedulerProvider) {

            override fun saveCallResult(request: Placemarks) {
                vehiclesDao.deleteVehicles()
                vehiclesDao.saveVehicles(request.placemarks)
            }

            override fun loadFromDb(): Flowable<List<Car>> {
                return vehiclesDao.getVehicles()
            }

            override fun createCall(): Single<Response<Placemarks>> {
                return networkService.getLocationsJson()
            }

        }.asFlowable()
    }
}
