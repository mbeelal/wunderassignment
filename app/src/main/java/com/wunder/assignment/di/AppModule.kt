package com.wunder.assignment.di

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.wunder.assignment.data.local.CarsDao
import com.wunder.assignment.data.local.CarsDatabase
import com.wunder.assignment.data.remote.NetworkService
import com.wunder.assignment.repository.CarsRepository
import com.wunder.assignment.util.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule {

    @Provides
    internal fun provideSchedulerProvider(): SchedulerProvider = SchedulerProvider()

    @Provides
    @Singleton
    fun provideApplication(application: Application): Context = application

    @Provides
    @Singleton
    fun provideMovieDatabase(application: Context) =
            Room.databaseBuilder(application, CarsDatabase::class.java, "vehicles.db").build()

    @Provides
    @Singleton
    fun provideMovieDao(vehiclesDatabase: CarsDatabase) = vehiclesDatabase.getVehiclesDao()

    @Provides
    @Singleton
    fun provideRepository(networkService: NetworkService,
                          vehiclesDao: CarsDao,
                          context: Context,
                          schedulerProvider: SchedulerProvider) = CarsRepository(networkService, vehiclesDao, context, schedulerProvider)

}