package com.wunder.assignment.di

import com.wunder.assignment.view.map.MapsFragment
import com.wunder.assignment.view.vehicles.CarsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    internal abstract fun contributeVehiclesFragment(): CarsFragment

    @ContributesAndroidInjector
    internal abstract fun contributeMapFragment(): MapsFragment
}