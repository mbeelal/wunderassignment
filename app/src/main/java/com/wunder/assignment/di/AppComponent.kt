package com.wunder.assignment.di

import android.app.Application
import com.wunder.assignment.WunderApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidInjectionModule::class), (ViewModelModule::class),
    (ActivityModule::class), (NetworkModule::class), (AppModule::class)])

interface AppComponent {

    @Component.Builder
    interface Builder{

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: WunderApplication)
}