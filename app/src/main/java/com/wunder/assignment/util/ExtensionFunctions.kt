package com.wunder.assignment.util

import android.content.Context
import android.net.ConnectivityManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLngBounds

fun Context.isNetworkAvailable(): Boolean {
    val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
    connectivityManager?.let {
        it.activeNetworkInfo?.let {
            if (it.isConnected) return true
        }
    }
    return false
}

fun GoogleMap.animateCameraToBound(builder: LatLngBounds.Builder) {
    val bounds = builder.build()
    val cu = CameraUpdateFactory.newLatLngBounds(bounds, 15)
    animateCamera(cu)
}