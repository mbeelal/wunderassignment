package com.wunder.assignment.util

import android.content.Context
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

abstract class NetworkBoundResource<ResultType, RequestType>(context: Context, schedulerProvider: SchedulerProvider) {

    private val result: Flowable<Resource<ResultType>>

    init {
        // Lazy disk observable.
        val diskObservable = Flowable.defer {
            // Read from disk on Computation Scheduler
            loadFromDb().subscribeOn(schedulerProvider.getComputationThreadScheduler())
        }

        // Lazy network observable.
        val networkObservable = Flowable.defer {
            // Request API on IO Scheduler
            // Read/Write to disk on Computation Scheduler
            createCall().compose(schedulerProvider.ioToComputationSingleScheduler())
                    .observeOn(Schedulers.computation()).toFlowable()
                    .doOnNext { request: Response<RequestType> ->
                        if (request.isSuccessful) {
                            saveCallResult(processResponse(request))
                        }
                    }
                    //get the data from db with id of car so it becomes easy as we select an item
                    // from list then we can get the details of Car using the id
                    .flatMap { loadFromDb() }
        }

        //IF network is available fetch fresh data else from Db
        result = when {
            context.isNetworkAvailable() -> networkObservable
                    .map<Resource<ResultType>> { Resource.Success(it) }
                    .onErrorReturn { Resource.Failure(it) }
                    // Read results in Android Main Thread (UI)
                    .observeOn(schedulerProvider.getMainThreadScheduler())
                    .startWith(Resource.Loading())
            else -> diskObservable
                    .map<Resource<ResultType>> { Resource.Success(it) }
                    .onErrorReturn { Resource.Failure(it) }
                    // Read results in Android Main Thread (UI)
                    .observeOn(schedulerProvider.getMainThreadScheduler())
                    .startWith(Resource.Loading())
        }
    }

    fun asFlowable(): Flowable<Resource<ResultType>> {
        return result
    }

    private fun processResponse(response: Response<RequestType>): RequestType {
        return response.body()!!
    }

    protected abstract fun saveCallResult(request: RequestType)

    protected abstract fun loadFromDb(): Flowable<ResultType>

    protected abstract fun createCall(): Single<Response<RequestType>>
}