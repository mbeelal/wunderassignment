package com.wunder.assignment.util

import io.reactivex.FlowableTransformer
import io.reactivex.Scheduler
import io.reactivex.SingleTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Helper class for Rx to get required thread
 */
class SchedulerProvider {

    fun <T> ioToComputationSingleScheduler(): SingleTransformer<T, T> = SingleTransformer { transformer ->
        transformer.subscribeOn(getIOThreadScheduler())
                .observeOn(getComputationThreadScheduler())
    }

    fun <T> ioToMainFlowableScheduler(): FlowableTransformer<T, T> = FlowableTransformer { transformer ->
        transformer.subscribeOn(getIOThreadScheduler())
                .observeOn(getMainThreadScheduler())
    }

    fun <T> ioToMainSingleScheduler(): SingleTransformer<T, T> = SingleTransformer { transformer ->
        transformer.subscribeOn(getIOThreadScheduler())
                .observeOn(getMainThreadScheduler())
    }

    private fun getIOThreadScheduler() = Schedulers.io()

    fun getComputationThreadScheduler() = Schedulers.computation()

    fun getMainThreadScheduler(): Scheduler = AndroidSchedulers.mainThread()
}