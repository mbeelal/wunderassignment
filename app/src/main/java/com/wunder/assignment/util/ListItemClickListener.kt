package com.wunder.assignment.util

interface ListItemClickListener {
    fun onItemClick(id: Int)
}