package com.wunder.assignment.view.vehicles


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.android.databinding.library.baseAdapters.BR
import com.wunder.assignment.MainActivity
import com.wunder.assignment.R
import com.wunder.assignment.data.remote.model.Car
import com.wunder.assignment.databinding.FragmentCarsBinding
import com.wunder.assignment.util.ListItemClickListener
import com.wunder.assignment.view.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_cars.*

class CarsFragment : BaseFragment<FragmentCarsBinding, CarsViewModel>() {

    override fun getBindingVariable() = BR.viewModel

    override fun getLayoutId() = R.layout.fragment_cars

    override fun getViewModel() = ViewModelProviders.of(requireActivity(), viewModelFactory).get(CarsViewModel::class.java)

    companion object {
        fun newInstance(): CarsFragment {
            return CarsFragment()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observerViewModel()
    }

    private fun observerViewModel() {
        viewModel.getCarsData().observe(this@CarsFragment, Observer { it ->
            it?.let {
                initAndSetData(it)
            } ?: kotlin.run {
                //TODO: show error on UI
            }
        })
    }

    private fun initAndSetData(products: List<Car>) {
        rv_cars.apply {
            layoutManager = LinearLayoutManager(this@CarsFragment.context)
            val adapter = CarsListAdapter(products)
            adapter.setOnClickListener(clickListener)
            this.adapter = adapter
        }
    }

    private val clickListener = object : ListItemClickListener {
        override fun onItemClick(id: Int) {
            viewModel.setCarIdToLoad(id)
            (activity as MainActivity).openMapFragment()
        }
    }
}
