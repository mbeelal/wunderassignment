package com.wunder.assignment.view.map

import android.arch.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.wunder.assignment.data.remote.model.Car
import com.wunder.assignment.repository.CarsRepository
import com.wunder.assignment.util.SchedulerProvider
import com.wunder.assignment.view.base.BaseViewModel
import javax.inject.Inject


@Deprecated("Instead of using two viewodels, These fragments can share a ViewModel using their activity scope to handle the communication" +

        "This approach offers the following benefits: 1) The activity does not need to do anything, or know anything about this communication." +
        "2) Fragments don't need to know about each other besides the SharedViewModel contract. If one of the fragments disappears, the other one keeps working as usual." +
        "3)Each fragment has its own lifecycle, and is not affected by the lifecycle of the other one. If one fragment replaces the other one, the UI continues to work without any problems.)")

// "https://developer.android.com/topic/libraries/architecture/viewmodel#sharing"
class MapViewModel @Inject constructor(private val repository: CarsRepository, private val  schedulerProvider: SchedulerProvider): BaseViewModel() {

    private val markerOptions: MutableLiveData<List<MarkerOptions>> by lazy { MutableLiveData<List<MarkerOptions>>() }

    private val vehicles: MutableLiveData<List<Car>> by lazy { MutableLiveData<List<Car>>() }

    private fun getFromDb(): MutableLiveData<List<Car>> {
        mDisposable.add(repository.getVehiclesFromDatabase()
                .compose(schedulerProvider.ioToMainFlowableScheduler())
                .doOnSubscribe {
                    isLoading.set(true)
                }
                .subscribe({
                    isLoading.set(false)
                    vehicles.value = it
                }, {
                    isLoading.set(false)
                    it.printStackTrace()
                }))

        return vehicles
    }

    fun getVehiclesData(carId: Int = -1): MutableLiveData<List<Car>> {
        if (carId != -1) {
            return getVehicleDataByID(carId)
        }
        return if (vehicles.value == null) {
            getFromDb()
        } else {
            vehicles
        }
    }

    private fun getVehicleDataByID(id: Int): MutableLiveData<List<Car>> {
        val car = MutableLiveData<List<Car>>()
       /* mDisposable.add(repository.getVehicleById(id)
                .compose(schedulerProvider.ioToMainSingleScheduler())
                .doOnSubscribe {
                    isLoading.set(true)
                }
                .subscribe({
                    isLoading.set(false)
                    car.value = it
                }, {
                    isLoading.set(false)
                    it.printStackTrace()
                }))*/

        return car
    }

    //prepare makers for all locations
    internal fun prepareMarkerOptions(vehicles: List<Car>) {
        var latLng: LatLng

        val markerOptions = ArrayList<MarkerOptions>()

        for (vehicle in vehicles) {
            //Lat lan or reversed in data so reading accordingly
            latLng = LatLng(vehicle.coordinates[1], vehicle.coordinates[0])

            //set name to show on when click on marker
            markerOptions.add(MarkerOptions().position(latLng).title(vehicle.name))
        }

        this.markerOptions.value = markerOptions
    }

    fun getMarkersOptions() = markerOptions
}