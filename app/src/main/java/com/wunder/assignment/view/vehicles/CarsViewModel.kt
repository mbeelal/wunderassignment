package com.wunder.assignment.view.vehicles

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.wunder.assignment.repository.CarsRepository
import com.wunder.assignment.data.remote.model.Car
import com.wunder.assignment.util.Resource
import com.wunder.assignment.util.SingleEvent
import com.wunder.assignment.view.base.BaseViewModel
import io.reactivex.exceptions.OnErrorNotImplementedException
import javax.inject.Inject
class CarsViewModel @Inject constructor(private val repository: CarsRepository): BaseViewModel() {

    @get:JvmName("carsData")
    private val carsData: MutableLiveData<List<Car>> by lazy { MutableLiveData<List<Car>>() }

    private val markerOptions: MutableLiveData<List<MarkerOptions>> by lazy { MutableLiveData<List<MarkerOptions>>() }

    private var selectCarData: LiveData<Car> = MutableLiveData()

    @get:JvmName("carIdToLoad")
    private val carIdToLoad: MutableLiveData<SingleEvent<Int>> by lazy { MutableLiveData<SingleEvent<Int>>() }

    init {
        //Will be called once #carIdToLoad value is updated with selected car from the list
        selectCarData = Transformations.switchMap(carIdToLoad) {
            carIdToLoad.value?.peekContent()?.run {
                repository.getVehicleById(this)
            }
        }


        mDisposable.add(repository.loadVehiclesData().subscribe({
            when (it) {
                is Resource.Loading -> {
                    isLoading.set(true)
                }

                is Resource.Success -> {
                    isLoading.set(false)
                    carsData.value = it.data
                }

                is Resource.Failure -> {
                    //TODO: handling/showing different errors types
                    it.throwable.printStackTrace()
                    isLoading.set(false)
                }

                else -> throw IllegalStateException("State not known or implemented.")
            }
        }, { t: Throwable ->
            throw OnErrorNotImplementedException(t) // Explicitly throw this exception to debug.
        }))
    }

    fun getCarsData() = carsData

    //prepare makers for all locations
    fun prepareMarkerOptions(vehicles: List<Car>){
        var latLng: LatLng

        val markerOptionsList = ArrayList<MarkerOptions>()

        for (vehicle in vehicles) {
            //Lat lan or reversed in data so reading accordingly
            latLng = LatLng(vehicle.coordinates[1], vehicle.coordinates[0])

            //set name to show on when click on marker
            markerOptionsList.add(MarkerOptions().position(latLng).title(vehicle.name))
        }

        markerOptions.value = markerOptionsList
    }

    fun getMarkersOptions() = markerOptions

    fun getSelectCarData() = selectCarData

    //CLick event should be handled only once clicked, so made this a single event
    fun setCarIdToLoad(id: Int) {
        carIdToLoad.value = SingleEvent(id)
    }

    fun getCarIdToLoad() = carIdToLoad
}

