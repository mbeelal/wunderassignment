package com.wunder.assignment.view.base

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableField
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel: ViewModel() {

    internal val mDisposable: CompositeDisposable by lazy { CompositeDisposable() }

    val isLoading = ObservableField<Boolean>()

    override fun onCleared() {
        mDisposable.clear()
        mDisposable.dispose()

        super.onCleared()
    }
}