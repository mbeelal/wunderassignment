package com.wunder.assignment.view.map


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.wunder.assignment.BR
import com.wunder.assignment.R
import com.wunder.assignment.databinding.FragmentMapBinding
import com.wunder.assignment.util.animateCameraToBound
import com.wunder.assignment.view.base.BaseFragment
import com.wunder.assignment.view.vehicles.CarsViewModel

class MapsFragment : BaseFragment<FragmentMapBinding, CarsViewModel>(), OnMapReadyCallback,
        GoogleMap.OnMapLoadedCallback, GoogleMap.OnCameraIdleListener{

    private val mapFragment: SupportMapFragment by lazy { SupportMapFragment.newInstance() }
    private lateinit var googleMap: GoogleMap

    private lateinit var selectCarMarker: Marker
    private var selectedCarId: Int = -1

    companion object {
        fun newInstance(): MapsFragment {
            return MapsFragment()
        }
    }

    override fun getBindingVariable() = BR.viewModel

    override fun getLayoutId() = R.layout.fragment_map

    override fun getViewModel() = ViewModelProviders.of(requireActivity(), viewModelFactory).get(CarsViewModel::class.java)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        childFragmentManager.beginTransaction().add(R.id.map, mapFragment).commit()
        mapFragment.getMapAsync(this)

        observeViewModel()
    }

    private fun observeViewModel() {
        //If car is selected from list, next time wont get observed untill click again as content has been handled
       viewModel.getCarIdToLoad().value?.getContentIfNotHandled()?.let {
            selectedCarId = it
            viewModel.getSelectCarData().observe(requireActivity(), Observer { car ->
                car?.run {
                    //to use the same methods of creating marker options and adding marker on map
                    // converting this single selected car to list
                    viewModel.prepareMarkerOptions(listOf(car))
                }
            })
           // For maps button click
        } ?: kotlin.run {
           // As Viewmodel is shared so data is persisted and no need to fetch again the same data again
            viewModel.getCarsData().observe(this@MapsFragment, Observer { list ->
                viewModel.prepareMarkerOptions(list!!)
            })
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap

        this.googleMap.setOnMapLoadedCallback(this@MapsFragment)
        this.googleMap.setOnCameraIdleListener (this@MapsFragment)
    }

    override fun onMapLoaded() {
        addMarkers()
    }

    override fun onCameraIdle() {
        if (::selectCarMarker.isInitialized) {
            selectCarMarker.showInfoWindow()
        }
    }

    private fun addMarkers() {
        viewModel.getMarkersOptions().observe(this@MapsFragment, Observer { it ->
            val builder = LatLngBounds.Builder()
            var marker: Marker
            it?.forEach { markerOption ->
                marker = googleMap.addMarker(markerOption)
                builder.include(marker.position)

                //if car selected from list keep the marker to show info window
                if (it.size == 1 && selectedCarId != -1) {
                    selectCarMarker = marker
                }
            }
            //move camera to bounds of markers
            googleMap.animateCameraToBound(builder)
        })
    }
}
