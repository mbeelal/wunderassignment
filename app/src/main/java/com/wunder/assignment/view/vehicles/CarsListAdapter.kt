package com.wunder.assignment.view.vehicles

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.wunder.assignment.R
import com.wunder.assignment.data.remote.model.Car
import com.wunder.assignment.databinding.ItemCarBinding
import com.wunder.assignment.util.ListItemClickListener

class CarsListAdapter(private val dataList: List<Car>) : RecyclerView.Adapter<CarsListAdapter.ViewHolder>() {

    private var clickListener: ListItemClickListener? = null

    fun setOnClickListener(clickListener: ListItemClickListener) {
        this.clickListener = clickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ItemCarBinding>(LayoutInflater.from(parent.context),
                R.layout.item_car, parent, false)

        return ViewHolder(binding, clickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(dataList[position])
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    inner class ViewHolder(private var binding: ItemCarBinding, private val listener: ListItemClickListener?) : RecyclerView.ViewHolder(binding.root) {

        fun bindData(product: Car) {
            binding.car = product
            listener?.let {
                binding.clickListener = listener
            }
        }
    }
}