package com.wunder.assignment

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.wunder.assignment.CustomMatchers.Companion.withItemCount
import com.wunder.assignment.R.id.rv_cars
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CarsFragmentTest {

    @Rule @JvmField
    val activityTestRule = ActivityTestRule(MainActivity::class.java)


    @Test
    fun testName() {
        //onView(withId(rv_cars)).check(matches(withItemCount(423)))

        val recyclerView = activityTestRule.activity.findViewById<RecyclerView>(R.id.rv_cars)
        val itemCount = recyclerView.adapter?.itemCount?.minus(1)

        onView(withId(R.id.rv_cars))
                .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(itemCount!!))
    }
}