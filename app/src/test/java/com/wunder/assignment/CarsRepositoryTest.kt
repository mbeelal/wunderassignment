package com.wunder.assignment

import com.wunder.assignment.data.remote.NetworkService
import com.wunder.assignment.data.remote.model.Placemarks
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

class CarsRepositoryTest {

    @Mock
    private lateinit var netWorkService: NetworkService

    @get:Rule
    val rxSchedulerRule = RxSchedulerRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun testEmptyCarsList() {

        Mockito.`when`(netWorkService.getLocationsJson()).thenReturn(Single.just(Response.success(Placemarks(listOf()))))


        val testObserver = netWorkService.getLocationsJson().test()

        testObserver.assertValue { testObserver.values()[0].body()?.placemarks?.size == 0 }
    }
}