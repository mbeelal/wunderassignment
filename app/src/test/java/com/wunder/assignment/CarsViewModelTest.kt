package com.wunder.assignment

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth
import com.wunder.assignment.data.remote.model.Car
import com.wunder.assignment.repository.CarsRepository
import com.wunder.assignment.util.Resource
import com.wunder.assignment.view.vehicles.CarsViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit

class CarsViewModelTest {

    @get:Rule
    val mockitoRule = MockitoJUnit.rule()

    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val rxSchedulerRule = RxSchedulerRule()

    @Mock
    private lateinit var repository: CarsRepository

    @Mock
    private lateinit var classUnderTest: CarsViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        classUnderTest = CarsViewModel(repository)
    }

    @Test
    fun `init set joke list to empty`() {
        val liveDataUnderTest = classUnderTest.getCarsData().testObserver()
        //classUnderTest.getCarsData()

        Truth.assert_()
                .that(liveDataUnderTest.observedValues)
                .isEqualTo(Resource.Loading<List<Car>>())
    }


}